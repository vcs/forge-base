#
FROM gitlab-registry.cern.ch/linuxsupport/cc7-base
#
MAINTAINER Alvaro Gonzalez <agonzale@cern.ch>
#
# Copy the repository for oracle-instantclient-tnsnames.ora
#
COPY *.repo /etc/yum.repos.d/

#
# Python modules
#
COPY requirements.txt .

ENV PATH=/opt/rh/rh-python36/root/usr/bin/:$PATH

RUN INSTALL_PKGS="rh-python36-python oracle-instantclient-tnsnames.ora oracle-instantclient12.2-basic oracle-instantclient12.2-meta afs_tools_standalone nss_wrapper openssh-clients libmemcached-devel httpd MySQL-python wassh-ssm-cern wassh uglify-js" && \
    INSTALL_PKGS_BUILD="gcc rh-python36-python-devel httpd-devel openldap-devel zlib-devel" && \
    yum install -y centos-release-scl && \
    yum update -y && \
    yum install -y --setopt=tsflags=nodocs --enablerepo=centosplus $INSTALL_PKGS && \
    # Only for pip build/install
    yum install -y $INSTALL_PKGS_BUILD && \
    #
    # Remove centos-logos (httpd dependency, ~20MB of graphics) to keep image
    # size smaller.
    rpm -e --nodeps centos-logos && \
    pip install --upgrade && \
    pip install -r requirements.txt && \
    # This is the cleanest way I can think to do this, still feels dirty
    ln -s $(mod_wsgi-express module-location) /etc/httpd/modules/mod_wsgi.so && \
    # Remove requirements.txt, only used for install
    rm requirements.txt && \
    yum remove -y $INSTALL_PKGS_BUILD && \
    yum clean all -y && \
    rm -rf /var/cache/yum /var/log/anaconda/ /var/lib/yum/ \
    /var/lib/rpm/ /root/.cache/ && \
    # Create folder for the code and the home for the user to run the app
    mkdir /var/django/ && mkdir -p /var/django/home/ && \
    mkdir -p /usr/local/bin/ && \
    mkdir -p /run/httpd/ && chown -R root:root /run/httpd && chmod a+rw /run/httpd

#
# Copy command to exec in the entry point
#
COPY httpd-foreground /usr/local/bin/

#
# Copy entrypoint
#
#COPY entrypoint.sh /

#
# Copy the configuration
#
COPY httpd.conf /etc/httpd/conf/

#
# Run the application (port 8000)
#
EXPOSE 8000

WORKDIR /var/django/

#ENTRYPOINT '/entrypoint.sh'
