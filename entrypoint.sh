#!/bin/sh


# Use nss_wrapper to add the current user to /etc/passwd
# and enable the use of tools like ssh
USER_ID=$(id -u)
GROUP_ID=$(id -g)
# Pointless if running as root
if [[ "${USER_ID}" != '0' ]]; then
   export NSS_WRAPPER_PASSWD=/tmp/nss_passwd
   export NSS_WRAPPER_GROUP=/tmp/nss_group
   cp /etc/passwd $NSS_WRAPPER_PASSWD
   cp /etc/group  $NSS_WRAPPER_GROUP

   if ! getent passwd "${USER_ID}" >/dev/null; then
      # we need an entry in passwd for current user. Make sure there is no conflict
      sed -e '/^forge:/d' -i $NSS_WRAPPER_PASSWD
      echo "forge:x:${USER_ID}:${GROUP_ID}:Rundeck instance:/var/django/home:/sbin/nologin" >> $NSS_WRAPPER_PASSWD
   fi
   export LD_PRELOAD=libnss_wrapper.so
 fi

/var/django/manage.py collectstatic --noinput

if [ $? -ne 0 ]; then
    echo "Error while running collectstatic" >&2
fi

/var/django/manage.py setsuperusers

if [ $? -ne 0 ]; then
    echo "Error while running setsuperusers" >&2
fi

exec "httpd-foreground"
